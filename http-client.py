#!/usr/bin/python3
import sys
import requests
from urllib.parse import urlparse

url = sys.argv[1]
req = requests.get(url)

path = urlparse(url).path
if path == "": path = "/"

print("\n### HTTP Viesti ###\n")

print("GET {} HTTP/1.1".format(path))
for header in req.request.headers: 
  print(header+": "+req.request.headers[header])

print('\n### HTTP Vastaus ###\n')

print("HTTP/1.1 {}".format(req.status_code))
for header in req.headers: 
  print(header+": "+req.headers[header])
